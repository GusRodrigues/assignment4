﻿using NUnit.Framework;
using System;

namespace Assignment4.Tests
{
    class SeatTest
    {
        public ISeat seat;

        [Test]
        public void TestEmptySeat()
        {
            // assemble
            seat = new UnassignedSeat();
            // act
            bool actual = seat.IsEmpty;
            bool person = String.IsNullOrWhiteSpace(seat.Person.Name);

            // assert
            Assert.That(actual, Is.True);
            Assert.That(person, Is.True);

        }
        [Test]
        public void Test_SeatIsOccupied()
        {
            // assemble
            Person person = new Person("John Doe");
            seat = new AssignedSeat(person);
            // act
            bool actual = seat.IsEmpty;

            // assert
            Assert.That(actual, Is.False);
            Assert.That(String.IsNullOrWhiteSpace(seat.Person.Name), Is.False);
        }
        [Test]
        public void Test_AssignedSeatIsImmutable()
        {
            // assemble
            Person oldPerson = new Person("John Doe");
            Person person = oldPerson;
            seat = new AssignedSeat(person);
            // act
            person = new Person("Jane Doe");
            // Assert
            Assert.That(seat.Person, Is.EqualTo(oldPerson));
            Assert.That(seat.Person.Equals(person), Is.False);
        }


    }
}
