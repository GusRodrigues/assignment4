using NUnit.Framework;
using Assignment4;

namespace Assignment4.Tests
{
    public class Tests
    {
        public Person person;

        [SetUp]
        public void Setup()
        {
            person = new Person("John Doe");
        }

        [Test]
        public void TestPersonImmutability()
        {
            // assemble
            string name = "John Doe";
            string mutateName = "James Dio";

            // act
            person = new Person(name);
            // assert
            Assert.AreEqual(name, person.Name);
            // act again
            name = mutateName;

            // assert
            Assert.AreNotEqual(name, person.Name);
            Assert.AreEqual("John Doe", person.Name);
            
        }

        [Test]
        public void TestEquality()
        {
            // Assemble
            Person person2 = new Person("John Doe");
            Person person3 = new Person("John Doe");

            // act
            bool reflexive = person.Equals(person); // a == a
            bool symetric = (person.Equals(person2) && person2.Equals(person)); // a == b && b == a
            bool transitive = (person.Equals(person2) == person2.Equals(person3) && person.Equals(person3)); // (a == b) == (b == c) && a == c
     
            // assert
            // reflexive equality
            Assert.IsTrue(reflexive);
            // symmetric
            
            Assert.IsTrue(symetric);
            
            // transitive
            Assert.IsTrue(transitive);
        }

        [Test]
        public void TestHashing()
        {
            // Assemble
            int hash1 = person.GetHashCode();
            int hash2 = new Person("Jane Doe").GetHashCode();

            // act
            bool equalHash = hash1 == person.GetHashCode(); // a == a
            bool unequalHash = hash1 == hash2;

            // assert
            Assert.That(equalHash, Is.EqualTo(true));
            Assert.That(unequalHash, Is.EqualTo(false));
        }
    }
}