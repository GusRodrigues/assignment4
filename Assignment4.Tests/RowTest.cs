﻿using NUnit.Framework;
using System;

namespace Assignment4.Tests
{
    class RowTest
    {
        public Row Row;
        [SetUp]
        public void Setup() => Row = new Row();

        [Test]
        public void Test_IsRowEmpty()
        {
            // Assemble & Act
            bool actual = Row.IsEmpty;
            // Assert
            Assert.That(actual, Is.True);
        }

        [Test]
        public void Test_AssignSeat()
        {
            // act
            Row secondRow = Row.AssignSeat(0, new Person("wile e coyote"));
            // 
            Assert.That(secondRow.IsEmpty, Is.False);
            Assert.That(secondRow.Equals(Row), Is.False);
        }

        [Test]
        public void Test_AssignNotEmptySeat()
        {
            // Assemble
            Row = Row.AssignSeat(0, new Person("Road Runner"));
            // Assert
            Assert.Throws<InvalidOperationException>(() => Row.AssignSeat(0, new Person("wile e coyote")));

        }
        [Test]
        public void Test_RemoveFromSeat()
        {
            // Assemble
            Row = Row.AssignSeat(0, new Person("Road Runner"));
            // Act
            Row = Row.RemoveFromSeat(0);
            // Assert
            Assert.That(Row.IsEmpty, Is.True);

        }

        [Test]
        public void Test_RemoveEmptySeat()
        {
            // Assert and Act
            Assert.Throws<InvalidOperationException>(() => Row.RemoveFromSeat(0));

        }

        [Test]
        public void Test_IfCanChangeRowFromOutside()
        {
            // assemble
            var emptySeat = new UnassignedSeat();
            var occupiedSeat = new AssignedSeat(new Person("Jane Doe"));
            char column = 'A';
            // act & assert

            Assert.Throws<ArgumentException>(() => { Row.Seats.Add(column, occupiedSeat); });

            Assert.That(Row.Seats[column].IsEmpty, Is.True);
        }

        [Test]
        public void Test_CounterZeroForEmptyRow()
        {
            // Assemble
            // act
            int actual = Row.SeatedCounter();
            // Assert
            Assert.That(actual, Is.Zero);

        }
        [Test]
        public void Test_CounterOneForOneSeatAssigned()
        {
            Row = Row.AssignSeat(2, new Person("Connor"));
            // Assemble
            int expected = 1;
            // Act
            int actual = Row.SeatedCounter();
            // Assert
            Assert.That(actual, Is.Positive);
            Assert.That(actual, Is.InRange(1, 4));
            Assert.That(expected, Is.EqualTo(Row.SeatedCounter()));

        }
        [Test]
        public void Test_CounterFourForFullRow()
        {
            Row = Row.AssignSeat(0, new Person("John"));
            Row = Row.AssignSeat(1, new Person("Mike"));
            Row = Row.AssignSeat(2, new Person("Connor"));
            Row = Row.AssignSeat(3, new Person("Andy"));
            // Assemble
            int expected = 4;
            // Act
            int actual = Row.SeatedCounter();
            // Assert
            Assert.That(actual, Is.Positive);
            Assert.That(actual, Is.InRange(1, 4));
            Assert.That(expected, Is.EqualTo(Row.SeatedCounter()));

        }
        [Test]
        public void Test_CountTwoForTwoSeaters()
        {
            Row = Row.AssignSeat(0, new Person("John"));
            Row = Row.AssignSeat(3, new Person("Andy"));
            // Assemble
            int expected = 2;
            // Act
            int actual = Row.SeatedCounter();
            // Assert
            Assert.That(actual, Is.Positive);
            Assert.That(actual, Is.InRange(1, 2));
            Assert.That(expected, Is.EqualTo(Row.SeatedCounter()));
        }
    }
}
