﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment4.Tests
{
    class InputParserTest
    {

        [Test]
        public void Test_StringReturnValidInt()
        {
            // Assemble
            string test = "1";
            int expected = 1;
            // Act
            int actual = InputParser.ParseStringToInt(test);
            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Test_NullWillThrowException()
        {
            // Assemble
            // Act
            // Assert
            Assert.That(() => InputParser.ParseStringToInt(null),
                Throws.TypeOf<ArgumentNullException>()
                .With.Message.EqualTo("Value cannot be null. (Parameter 'input')")
                );
            ;
        }

        [Test]
        public void Test_LargeNumberWillThrowException()
        {
            // Assemble
            // Act
            // Assert
            Assert.That(() => InputParser.ParseStringToInt("111111111111111111111111111111111111111111111111111111"),
                Throws.TypeOf<OverflowException>()
                .With.Message.EqualTo("Cannot handle this number.")
                );
            ;
        }
        [Test]
        public void Test_VerySmallNumberWillThrowException()
        {
            // Assemble
            // Act
            // Assert
            Assert.That(() => InputParser.ParseStringToInt("-111111111111111111111111111111111111111111111111111111"),
                Throws.TypeOf<OverflowException>()
                .With.Message.EqualTo("Cannot handle this number.")
                );
            ;
        }

        [Test]
        public void Test_FirstMenuSelector()
        {
            // Assemble
            int expected = 1;
            List<string> texts = new List<string>{ "1", "one-", "one", "first" };
            // Act
            texts.ForEach(input => 
                    // Assert
                    Assert.That(() => InputParser.MenuSelector(input), Is.EqualTo(expected)
                ));
            
        }
        [Test]
        public void Test_InvalidMenuSelectorShouldReturnZero()
        {
            // Assemble
            List<string> texts = new List<string> { "-1", "minusone-", "minusone", "minusfirst" };
            // Act
            texts.ForEach(input =>
                    // Assert
                    Assert.That(() => InputParser.MenuSelector(input), Is.Zero
                ));

        }
                
        [Test]
        public void Test_RemovingseatMenuSelectorShouldReturnTwo()
        {
            // Assemble
            int expected = 2;
            List<string> texts = new List<string> { "2", "remove", "2", "two-", "two", "second", "second" };
            // Act
            texts.ForEach(input =>
                    // Assert
                    Assert.That(() => InputParser.MenuSelector(input), Is.EqualTo(expected)
                ));

        }

        [Test]
        public void Test_PassengerListMenuSelectorShouldReturnThree()
        {
            // Assemble
            int expected = 3;
            List<string> texts = new List<string> { "3", "list", "3", "three-", "three", "third" };
            // Act
            texts.ForEach(input =>
                    // Assert
                    Assert.That(() => InputParser.MenuSelector(input), Is.EqualTo(expected)
                ));

        }

        [Test]
        public void Test_SeatmapMenuSelectorShouldReturnFourForSeatmap()
        {
            // Assemble
            int expected = 4;
            List<string> texts = new List<string> { "4", "seatmap", "4-", "four-", "four", "fourth" };
            // Act
            texts.ForEach(input =>
                    // Assert
                    Assert.That(() => InputParser.MenuSelector(input), Is.EqualTo(expected)
                ));

        }

        [Test]
        public void Test_WhenQuitingMenuSelectorShouldReturnFive()
        {
            // Assemble
            int expected = 5;
            List<string> texts = new List<string> { "5", "exit", "bye", "adios", "5-", "fifth-", "five" };
            // Act
            texts.ForEach(input =>
                    // Assert
                    Assert.That(() => InputParser.MenuSelector(input), Is.EqualTo(expected)
                ));

        }


        [Test]
        public void Test_IfZeroShouldReturnFalseIfBiggerThanZero()
        {
            // Assemble
            int toTest = 0;
            // Act
            bool actual = InputParser.IsGreaterThanZero(toTest);
            // Assert
            Assert.That(actual, Is.False);
        }
        [Test]
        public void Test_IfMinusOneShouldReturnFalseIfBiggerThanZero()
        {
            // Assemble
            int toTest = -1;
            // Act
            bool actual = InputParser.IsGreaterThanZero(toTest);
            // Assert
            Assert.That(actual, Is.False);
        }
        [Test]
        public void Test_IfOneShouldReturnTrueIfBiggerThanZero()
        {
            // Assemble
            int toTest = 1;
            // Act
            bool actual = InputParser.IsGreaterThanZero(toTest);
            // Assert
            Assert.That(actual, Is.True);
        }
    }
}
