﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment4.Tests
{
    class AirplaneTest
    {
        public Airplane Airplane;

        [SetUp]
        public void Setup()
        {
            Airplane = new Airplane();
        }

        [Test]
        public void Test_HowManyRowsHas()
        {
            // Assemble
            int expected = 4;
            Row expectedRow = new Row();
            var expectedSeat = new UnassignedSeat();
            // Act
            int actual = Airplane.Rows.Count;
            var actualRow = Airplane.Rows[0].IsEmpty;
            var actualSeat = Airplane.Rows[0].Seats['A'];
            // Assert
            Assert.That(actual, Is.EqualTo(expected));
            Assert.That(actualRow, Is.EqualTo(expectedRow.IsEmpty));
            Assert.That(actualSeat, Is.EqualTo(expectedSeat));
        }

        [Test]
        public void Test_GetRowByNumber()
        {
            // Assemble
            int row = 3;
            int row2 = 0;
            // Act
            var actualRow = Airplane.GetRow(row);
            var actualSecondRow = Airplane.GetRow(row2);
            // Assert
            Assert.That(actualRow.IsEmpty, Is.True);
            Assert.That(actualSecondRow.IsEmpty, Is.True);
        }

        [Test]
        public void Test_ToTryGetRowOutsiedRange()
        {
            //
            Assert.That(
                () => Airplane.GetRow(5),
                Throws.Exception
                    .TypeOf<IndexOutOfRangeException>()
                    .With.Message
                    .EqualTo("invalid row")
                );
            Assert.That(
                () => Airplane.GetRow(10),
                Throws.Exception
                    .TypeOf<IndexOutOfRangeException>()
                    .With.Message
                    .EqualTo("invalid row")
                );

            Assert.That(
                () => Airplane.GetRow(4),
                Throws.Exception
                    .TypeOf<IndexOutOfRangeException>()
                    .With.Message
                    .EqualTo("invalid row")
                );
            Assert.That(
                () => Airplane.GetRow(-5),
                Throws.Exception
                    .TypeOf<IndexOutOfRangeException>()
                    .With.Message
                    .EqualTo("invalid row")
                );
        }

        [Test]
        public void Test_IfRowIsMutableOutsideAirplane()
        {
            // Assemble
            int row = 1;
            Person person = new Person("Road Runner");
            // act
            Airplane.Rows[row].AssignSeat(2, person);
            var actualRow = Airplane.Rows[row];
            // Assert
            Assert.That(actualRow.IsEmpty, Is.True);

        }

        [Test]
        public void Test_AssignSeatInARow()
        {
            // Assemble
            int row = 1;
            int label = 0;
            Airplane emptyAirplane = new Airplane();
            Person person = new Person("Road Runner");
            // Act
            Airplane = Airplane.AssignSeat(row, label, person);
            // Assert
            //Assert.That(Airplane.GetRow(row).Seats[Convert.ToChar(65 + label)].Person, Is.EqualTo(person));
            //Assert.That(Airplane.GetRow(row).Seats[Convert.ToChar(65 + label + 1)].IsEmpty, Is.True);
            Assert.That(emptyAirplane.Equals(Airplane), Is.False);
            //Assert.That(Airplane.PassengersCount(), Is.EqualTo(1));
        }
        [Test]
        public void Test_TryToDuplicateSeat()
        {
            // Act
            Airplane = Airplane.AssignSeat(1, 0, new Person("Road Runner"));
            //Assert
            Assert.That(
                () => Airplane.AssignSeat(1, 0, new Person("Road Runner")),
                Throws.Exception
                    .TypeOf<InvalidOperationException>()
                    .With.Message
                    .EqualTo("There is someone there!!!")
                );
        }


        [Test]
        public void Test_UnassignedUnseatedSeat()
        {
            // Assemble & Act & Assert
            Assert.That(
                () => Airplane.UnassignSeat(1, 0),
                        Throws.Exception
                            .TypeOf<InvalidOperationException>()
                            .With.Message
                            .EqualTo("No one is seated there!")

                );

        }
        [Test]
        public void Test_UnassignedAssignedSeat()
        {
            // Assemble
            int row = 1;
            int label = 0;
            Airplane = Airplane.AssignSeat(row, label, new Person("Road Runner"));

            // Act 
            Airplane = Airplane.UnassignSeat(row, label);
            //Assert
            Assert.That(Airplane.GetRow(row).Seats[Convert.ToChar(65 + label)].IsEmpty, Is.True);
        }

        [Test]
        public void Test_PassengerCountIsZeroWithEmptyAirplane()
        {
            // Assemble
            int expectet = 0;
            // Action
            int actual = Airplane.PassengersCount();
            // Assert
            Assert.That(actual, Is.Zero);
            Assert.That(expectet, Is.EqualTo(actual));
        }

        [Test]
        public void Test_PassengerCountIsOneWithOneAss()
        {
            // Assemble
            int expected = 1;
            // Action
            Airplane = Airplane.AssignSeat(1, 0, new Person("Road Runner"));
            int actual = Airplane.PassengersCount();
            // Assert
            Assert.That(actual, Is.Positive);
            Assert.That(expected, Is.EqualTo(actual));
        }
        [Test]
        public void Test_PassengerCountIsSixteenWithFullPlane()
        {
            // Assemble
            int expected = 16;
            // Action
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Airplane = Airplane.AssignSeat(i, j, new Person("Road Runner"));
                }

            }
            int actual = Airplane.PassengersCount();
            // Assert
            Assert.That(actual, Is.Positive);
            Assert.That(expected, Is.EqualTo(actual));
        }
        

        [Test]
        public void Test_RowPrinting()
        {
            // Assemble
            string expected = "1  A  B    C  D ";
                        
            // Act
            var actual = Airplane.DrawRow(1);
            // Assert
            Assert.That(expected, Is.EqualTo(actual));
        }

        [Test]
        public void Test_PrintRowTwo()
        {
            // Assemble
            // Assemble
            string expected = "2  A  B    C  D ";
            // Act
            var actual = Airplane.DrawRow(2);
            // Assert
            Assert.That(expected, Is.EqualTo(actual));
        }

        [Test]
        public void Test_InvalidRow()
        {
            // Assemble
            var expected = "Row must be equal or more than one and equal or less than 4";
            // Act

            // Assert
            Assert.That(() => Airplane.DrawRow(5),
                Throws.Exception
                .TypeOf<IndexOutOfRangeException>()
                .With.Message
                .EqualTo($"{expected}")
                );
            Assert.That(() => Airplane.DrawRow(0),
                Throws.Exception
                .TypeOf<IndexOutOfRangeException>()
                .With.Message
                .EqualTo($"{expected}")
                );
        }
        
        [Test]
        public void Test_FullRowPrinting()
        {
            // Assemble
            string expected = "1  -  -    -  - ";
            Airplane = Airplane.AssignSeat(0, 0, new Person("Johnny Test"));
            Airplane = Airplane.AssignSeat(0, 1, new Person("Johnny Test"));
            Airplane = Airplane.AssignSeat(0, 2, new Person("Johnny Test"));
            Airplane = Airplane.AssignSeat(0, 3, new Person("Johnny Test"));
            // Act
            var actual = Airplane.DrawRow(1);
            // Assert
            Assert.That(expected, Is.EqualTo(actual));
        }


        [Test]
        public void Test_EmptyPassengerList()
        {
            // Assemble
            List<string> expected = new List<string>();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    expected.Add($"{i + 1}{Convert.ToChar(65 + j)} - ");
                }
            }

            // Act
            List<string> actual = Airplane.PassengerList();
            // Assert
            actual.ForEach(item => Assert.That(expected.Contains(item), Is.True));
            Assert.That(Airplane.PassengersCount(), Is.Zero);
        }
        [Test]
        public void PassengerList()
        {
            // Assemble
            List<string> expected = new List<string>();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    var person = new Person("John Smith");
                    Airplane = Airplane.AssignSeat(i, j, person);
                    expected.Add($"{i + 1}{Convert.ToChar(65 + j)} - {person.Name}");
                }
            }

            // Act
            List<string> actual = Airplane.PassengerList();
            // Assert
            actual.ForEach(item => Assert.That(expected.Contains(item), Is.True));
            Assert.That(Airplane.PassengersCount(), Is.EqualTo(16));
        }
        
    }
}
