﻿/* Assignment 4
 * 
 * Revision History
 *      Gustavo Bonifacio Rodrigues, 2019.11.07: Created
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Assignment4
{
    class Program
    {
        static void Main(string[] args)
        {
            // This guarantees a neutral culture interface.
            // This runs the system under a Culture netral enviroment.
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            // Create The Airplane
            Menu.TopMenu(new Airplane());
        }
    }

    public static class Menu
    {
        public static void TopMenu(Airplane airplane)
        {
            Console.WriteLine("Choose an option:");
            Console.WriteLine("\t 1- Add Passenger");
            Console.WriteLine("\t 2- Remove Passenger");
            Console.WriteLine("\t 3- Full Passenger List");
            Console.WriteLine("\t 4- See seatmap");
            Console.WriteLine("\t 5- Exit");
            MonitorUserInput(Console.ReadLine(), airplane);
        }

        public static void MonitorUserInput(string input, Airplane airplane)
        {
            int selected;
            // Trims and convert the input in an upper case for easier parsing.
            input = input.Trim().ToLowerInvariant();
            selected = InputParser.MenuSelector(input);
            bool loop = true;
            do
            {

                switch (selected)
                {
                    case 1:
                        AddPassenger(airplane);
                        break;
                    case 2:
                        RemovePassenger(airplane);
                        break;
                    case 3:
                        PassengerList(airplane);
                        break;
                    case 4:
                        Seatmap(airplane);
                        break;
                    case 5:
                        Console.WriteLine("Good Bye!");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        TopMenu(airplane);
                        MonitorUserInput(Console.ReadLine(), airplane);
                        break;
                }

            } while (loop);
        }
        private static void AddPassenger(Airplane airplane)
        {
            Person person;
            int row;
            int label;
            Console.Clear();
            person = CreatePerson();
            row = SelectRow();
            label = SelectLabel();

            try
            {
                Console.Clear();
                airplane = airplane.AssignSeat((row-1), label, person);
                Console.WriteLine($"Assigned seat {row}{Convert.ToChar(65 + label)} to {person}");
            }
            catch (Exception)
            {
                Console.WriteLine($"Unable to assign {row}{Convert.ToChar(65 + label)} to {person}.");
                Console.WriteLine("Please, try again");
            }

            Console.WriteLine();

            TopMenu(airplane);
        }
        private static void RemovePassenger(Airplane airplane)
        {
            int row;
            int label;
            Console.Clear();
            row = SelectRow();
            label = SelectLabel();

            try
            {
                Console.Clear();
                Person person = airplane.GetRow(row).Seats[Convert.ToChar(65+label)].Person;
                airplane = airplane.UnassignSeat((row - 1), label);
                Console.WriteLine($"Removed {person} from seat {row}{Convert.ToChar(65 + label)}");
            }
            catch (Exception)
            {
                Console.WriteLine($"Unable to remove customer from {row}{Convert.ToChar(65 + label)}.");
                Console.WriteLine("Please, try again");
            }
            Console.WriteLine();

            TopMenu(airplane);
        }
        private static void PassengerList(Airplane airplane)
        {
            Console.Clear();
            Console.WriteLine($"List with all {airplane.PassengersCount()} passengers");
            airplane.PassengerList().ForEach(pax => Console.WriteLine(pax));
            Console.WriteLine();

            TopMenu(airplane);
        }
        private static void Seatmap(Airplane airplane)
        {
            Console.Clear();
            Console.WriteLine("Current Seatmap for this airplane.");
            Console.WriteLine(@"Occupied seats are shown with '-' symbol");
            Console.WriteLine("--------------------------------------");
            Console.WriteLine(airplane);
            Console.WriteLine();

            TopMenu(airplane);
        }
        private static Person CreatePerson()
        {
            string firstName;
            string lastName;
            Console.WriteLine("Type the first name:");
            firstName = Console.ReadLine();
            while (String.IsNullOrWhiteSpace(firstName))
            {
                Console.WriteLine("A name can't be empty spaced or null:");
                firstName = Console.ReadLine();
            }
            Console.WriteLine("Type the last name:");
            lastName = Console.ReadLine();
            while (String.IsNullOrWhiteSpace(lastName))
            {
                Console.WriteLine("A name can't be empty spaced or null:");
                lastName = Console.ReadLine();
            }

            return new Person(firstName + " " + lastName);
        }

        private static int SelectRow()
        {
            string input;
            Console.WriteLine("Type the row number:");
            input = Console.ReadLine();
            while (String.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine("Provide a valid number from 1 to 4:");
                input = Console.ReadLine();
            }
            return InputParser.ParseStringToInt(input);
        }
        private static int SelectLabel()
        {
            string input;
            Console.WriteLine("Type the seat (A/B/C/D):");
            input = Console.ReadLine();
            while (String.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine("Type the seat (A/B/C/D):");
                input = Console.ReadLine();
            }

            switch (input[0])
            {
                case 'a':
                    return 0;
                case 'b':
                    return 1;
                case 'c':
                    return 2;
                case 'd':
                    return 3;
                default:
                    throw new IndexOutOfRangeException();
            }

        }
    }

    /// <summary>
    /// <para>Airplane holds a set of Rows of Seats</para> 
    /// <para>Seat can be Unassigned, i.e. Unnocuppied or</para>
    /// <para>Seat can be Assigned, i.e. Ocuppied</para>
    /// Airplane is immutable
    /// </summary>
    public class Airplane
    {
        /// <summary>
        /// Readonly Field with Readonly list of Rows.
        /// </summary>
        public readonly IDictionary<int, Row> Rows;

        /// <summary>
        /// Creates an empty Airplane
        /// </summary>
        public Airplane()
        {
            Rows = _emptyPlane();
        }


        private Airplane(Dictionary<int, Row> rows)
        {
            Rows = new Dictionary<int, Row>(rows);
        }

        /// <summary>
        /// Assign that seat to that person
        /// </summary>
        /// <param name="row"></param>
        /// <param name="label"></param>
        /// <param name="person"></param>
        /// <returns></returns>
        /// <exception cref="IndexOutOfRangeException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public Airplane AssignSeat(int row, int label, Person person)
        {
            if (row > 3 || row < 0)
            {
                throw new IndexOutOfRangeException("invalid row");
            }

            Dictionary<int, Row> tempRows = new Dictionary<int, Row>(Rows);
            tempRows[row] = tempRows[row].AssignSeat(label, person);

            return new Airplane(tempRows);
        }
        public Airplane UnassignSeat(int row, int label)
        {
            if (row > 3 || row < 0)
            {
                throw new IndexOutOfRangeException("invalid row");
            }

            Dictionary<int, Row> tempRows = new Dictionary<int, Row>(Rows);
            tempRows[row] = tempRows[row].RemoveFromSeat(label);

            return new Airplane(tempRows);
        }

        /// <summary>
        /// Zero index
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public Row GetRow(int row)
        {

            if (row > 3 || row < 0)
            {
                throw new IndexOutOfRangeException("invalid row");
            }
            return Rows[row];
        }

        public int PassengersCount()
        {
            int counter = 0;
            foreach (var row in Rows.Keys)
            {
                counter += Rows[row].SeatedCounter();
            }
            return counter;
        }

        public override bool Equals(object obj)
        {
            return obj is Airplane airplane &&
                   EqualityComparer<IDictionary<int, Row>>.Default.Equals(Rows, airplane.Rows);
        }


        public override int GetHashCode()
        {
            return 1393792888 + EqualityComparer<IDictionary<int, Row>>.Default.GetHashCode(Rows);
        }

        /// <summary>
        /// <para>Will create four empty rows for the airplane</para>
        /// <para>The Rows are immutable</para>
        /// </summary>
        /// <returns></returns>
        private IDictionary<int, Row> _emptyPlane()
        {
            Dictionary<int, Row> temp = new Dictionary<int, Row>();
            for (int i = 0; i < 4; i++)
            {
                temp.Add(i, new Row());
            }
            return temp;

            /*
            var temp = new List<Row>(4);

            for (int i = 0; i < temp.Capacity; i++)
            {
                temp.Add(new Row());
            }
            return temp.AsReadOnly();*/
        }

        public string DrawRow(int row)
        {
            if (row < 1 || row > 4)
            {
                throw new IndexOutOfRangeException("Row must be equal or more than one and equal or less than 4");
            }
            var tempRow = GetRow(row - 1);
            StringBuilder sb = new StringBuilder();
            sb.Append($"{row} ");
            for (int i = 0; i < 4; i++)
            {
                char label = Convert.ToChar(i + 65);
                if (tempRow.Seats[label].IsEmpty)
                {
                    sb.Append($" {label} ");
                }
                else
                {
                    sb.Append(" - ");
                }
                if (i == 1)
                {
                    sb.Append("  ");
                }
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < Rows.Count; i++)
            {
                sb.AppendLine(DrawRow(i + 1));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Create passenger list
        /// </summary>
        /// <returns></returns>
        public List<string> PassengerList()
        {
            List<string> passengerList = new List<string>();
            for (int i = 0; i < Rows.Count; i++)
            {
                for (int j = 0; j < Rows[i].Seats.Count; j++)
                {
                    if (!Rows[i].Seats[Convert.ToChar(j + 65)].IsEmpty)
                    {
                        passengerList.Add($"{i + 1}{Convert.ToChar(65 + j)} - {Rows[i].Seats[Convert.ToChar(j + 65)].Person.Name}");
                    }
                    else
                    {
                        passengerList.Add($"{i + 1}{Convert.ToChar(65 + j)} - ");
                    }
                }
            }
            return passengerList;
        }
    }

    /// <summary>
    /// <para>A Row is Immutable</para>
    /// <para>Every row holds 4 seats.</para>
    /// </summary>
    public class Row
    {
        public readonly IDictionary<char, ISeat> Seats;

        public bool IsEmpty { get => _isEmpty(); }

        /// <summary>
        /// Creates an empty row.
        /// </summary>
        public Row()
        {
            Seats = EmptyRow();
        }
        private Row(IDictionary<char, ISeat> seats)
        {
            Seats = new Dictionary<char, ISeat>(seats);
        }

        /// <summary>
        /// <para>Assign a seat if that seat is not assigned and return a new Row with the customer seated on it.</para>
        /// <para>If seat is not empty, InvalidOperationException will be thrown</para>
        /// </summary>
        /// <param name="index">wich seat it will occupy</param>
        /// <param name="person">the person who will seat</param>
        /// <returns>A new Row with the customer seated on it</returns>
        /// <exception cref="System.InvalidOperationException">if the seat is occupied</exception>
        public Row AssignSeat(int index, Person person)
        {
            char column = Convert.ToChar(65 + index);
            var temp = new Dictionary<char, ISeat>(Seats);
            if (!temp[column].IsEmpty)
            {
                throw new InvalidOperationException("There is someone there!!!");
            }
            else
            {
                temp.Remove(column);
                temp.Add(column, new AssignedSeat(person));
            }

            return new Row(temp);
        }

        /// <summary>
        /// <para>Remove from the seat if the seat is occupied and return new Row withot the customer seated on it.</para>
        /// <para>If seat is empty, InvalidOperationException will be thrown</para>
        /// </summary>
        /// <param name="index">where it is seated</param>
        /// <returns>A new Row with the customer seated on it</returns>
        /// <exception cref="System.InvalidOperationException">if the seat is empty</exception>
        public Row RemoveFromSeat(int index)
        {
            char column = Convert.ToChar(65 + index);
            var temp = new Dictionary<char, ISeat>(Seats);
            if (temp[column].IsEmpty)
            {
                throw new InvalidOperationException("No one is seated there!");
            }
            else
            {
                temp.Remove(column);
                temp.Add(column, new UnassignedSeat());
            }
            return new Row(temp);
        }

        /// <summary>
        /// Creates a new row of empty seats assigning then to the chars A (65) to D (68)
        /// </summary>
        /// <returns></returns>
        private IDictionary<char, ISeat> EmptyRow()
        {
            var temp = new Dictionary<char, ISeat>();
            for (int i = 0; i < 4; i++)
            {
                char column = Convert.ToChar(i + 65);
                temp.Add(column, new UnassignedSeat());
            }

            return temp;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool _isEmpty()
        {
            int counter = 0;
            foreach (ISeat seat in Seats.Values)
            {
                if (seat.IsEmpty)
                {
                    counter++;
                }
            }
            return counter == Seats.Keys.Count;
        }

        public override bool Equals(object obj)
        {
            return obj is Row row &&
                   EqualityComparer<IDictionary<char, ISeat>>.Default.Equals(Seats, row.Seats) &&
                   IsEmpty == row.IsEmpty;
        }

        public override int GetHashCode()
        {
            var hashCode = -354561657;
            hashCode = hashCode * -1521134295 + EqualityComparer<IDictionary<char, ISeat>>.Default.GetHashCode(Seats);
            hashCode = hashCode * -1521134295 + IsEmpty.GetHashCode();
            return hashCode;
        }

        /// <summary>
        /// Counts how many seats are assigned in the Row
        /// </summary>
        /// <returns>How many Assigned seats</returns>
        public int SeatedCounter()
        {
            int counter = 0;
            foreach (char label in Seats.Keys)
            {
                if (!Seats[label].IsEmpty)
                {
                    counter++;
                }
            }
            return counter;
        }
    }
    public interface ISeat
    {
        bool IsEmpty { get; }
        Person Person { get; }
    }

    /// <summary>
    /// An Assigned seat has a person on it and False for is Empty
    /// </summary>
    public class AssignedSeat : ISeat
    {
        public bool IsEmpty { get => false; }

        public Person Person { get; }

        public AssignedSeat(Person person)
        {
            _ = person ?? throw new ArgumentNullException("A person need to be assigned to a seat");
            Person = person;
        }
        public override bool Equals(object obj)
        {
            return obj is ISeat seat &&
                   IsEmpty == seat.IsEmpty &&
                   EqualityComparer<Person>.Default.Equals(Person, seat.Person);
        }

        public override int GetHashCode()
        {
            var hashCode = 1704716174;
            hashCode = hashCode * -1521134295 + IsEmpty.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Person>.Default.GetHashCode(Person);
            return hashCode;
        }
    }

    /// <summary>
    /// An Unassigned Seat has no one assigned to that seat
    /// </summary>
    public class UnassignedSeat : ISeat
    {
        public bool IsEmpty { get => true; }

        public Person Person { get => new Person(""); }

        public override bool Equals(object obj)
        {
            return obj is ISeat seat &&
                   IsEmpty == seat.IsEmpty &&
                   EqualityComparer<Person>.Default.Equals(Person, seat.Person);
        }

        public override int GetHashCode()
        {
            var hashCode = 1704716174;
            hashCode = hashCode * -1521134295 + IsEmpty.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Person>.Default.GetHashCode(Person);
            return hashCode;
        }
    }

    /// <summary>
    /// Will ocuppy a seat
    /// </summary>
    public class Person
    {
        public string Name { get; }
        public Person(string name)
        {
            Name = name;
        }

        public override bool Equals(object obj)
        {
            return obj is Person person &&
                   Name == person.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public static class InputParser
    {

        /// <summary>
        /// Parse a string with number that will convert to integer.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="FormatException"></exception>
        /// <exception cref="OverflowException"></exception>
        public static int ParseStringToInt(string input)
        {
            try
            {
                EmptyOrWhiteSpacedInput(input);
                return Convert.ToInt32(int.Parse(input));
            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException("input");
            }
            catch (FormatException)
            {
                throw new FormatException("Need a numbers");
            }
            catch (OverflowException)
            {
                // less than System.UInt32.MinValue or greater than System.UInt32.MaxValue.
                throw new OverflowException("Cannot handle this number.");
            }
        }

        private static void EmptyOrWhiteSpacedInput(string input)
        {
            if (String.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentNullException();
            }
        }

        public static int MenuSelector(string input)
        {
            input = input.ToLowerInvariant();
            // first item
            if (input.Equals("1") || input.Equals("add") || input.StartsWith("1") || input.Equals("one-") || input.Equals("one") || input.Equals("first") || input.StartsWith("first"))
            {
                return 1;
            }
            // second item
            else if (input.Equals("2") || input.Equals("remove") || input.StartsWith("2") || input.Equals("two-") || input.Equals("two") || input.StartsWith("second") || input.Equals("second"))
            {
                return 2;
            }
            // third item
            else if (input.Equals("3") || input.Equals("list") || input.StartsWith("3") || input.Equals("three-") || input.Equals("three") || input.StartsWith("third") || input.Equals("third"))
            {
                return 3;
            }
            // fourth option
            else if (input.Equals("4") || input.Equals("seatmap") || input.StartsWith("4-") || input.Equals("four-") || input.Equals("four") || input.Equals("fourth"))
            {
                return 4;
            }
            else if (input.Equals("5") || input.Equals("exit") || input.Equals("bye") || input.Equals("adios") || input.Equals("5-") || input.Equals("fifth-") || input.Equals("five"))
            {
                return 5;
            }
            // none
            else
            {
                return 0;
            }
        }

        public static bool IsGreaterThanZero(int evaluate)
        {
            return evaluate >= 1.0;
        }

        /// <summary>
        /// Evaluates if a string may be larger than a int32 size.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>true if and only if the input can be greater than or equals the length of 32bit integer.</returns>
        public static bool IsToBigToParse(string input)
        {
            bool assess = false;
            try
            {
                if (input.Length >= Int32.MaxValue.ToString().Length)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("The value provide is too big for handle.");
                assess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                assess = true;
            }
            return assess;
        }

    }
}
